package com.gsb.suividevosfrais;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.BaseAdapter;

public class FraisHfAdapter extends BaseAdapter {

	ArrayList<FraisHf> lesFrais ; // liste des frais du mois
	LayoutInflater inflater ;
	Integer key ;  // annee et mois (cl� dans la liste)
	Context context ; // contexte pour g�rer la s�rialisation
	
	/**
	 * Constructeur de l'adapter pour valoriser les propri�t�s
	 * @param context
	 * @param lesFrais
	 * @param key
	 */
	public FraisHfAdapter(Context context, ArrayList<FraisHf> lesFrais, Integer key) {
		Log.d("tag", "FraisHfAdapter: passed " + lesFrais);
		inflater = LayoutInflater.from(context) ;
		this.lesFrais = lesFrais ;
		this.key = key ;
		this.context = context ;
	}
	
	/**
	 * retourne le nombre d'�l�ments de la listview
	 */
	@Override
	public int getCount() {
		if (lesFrais != null) {
			return lesFrais.size();
		}
		else {
			return  0;
		}
	}

	/**
	 * retourne l'item de la listview � un index pr�cis
	 */
	@Override
	public Object getItem(int index) {
		return lesFrais.get(index) ;
	}

	/**
	 * retourne l'index de l'�l�ment actuel
	 */
	@Override
	public long getItemId(int index) {
		return index;
	}

	/**
	 * structure contenant les �l�ments d'une ligne
	 */
	private class ViewHolder {
		TextView txtListJour ;
		TextView txtListMontant ;
		TextView txtListMotif ;
		Button   supprLigne;
	}
	
	/**
	 * Affichage dans la liste
	 */
	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		ViewHolder holder ;
		if (convertView == null) {
			holder = new ViewHolder() ;
			convertView = inflater.inflate(R.layout.layout_liste, null) ;
			holder.txtListJour = (TextView)convertView.findViewById(R.id.txtListJour) ;
			holder.txtListMontant = (TextView)convertView.findViewById(R.id.txtListMontant) ;
			holder.txtListMotif = (TextView)convertView.findViewById(R.id.txtListMotif) ;
			holder.supprLigne = (Button) convertView.findViewById(R.id.suprHF);
			convertView.setTag(holder) ;
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.txtListJour.setText(lesFrais.get(index).getJour().toString()) ;
		holder.txtListMontant.setText(lesFrais.get(index).getMontant().toString()) ;
		holder.txtListMotif.setText(lesFrais.get(index).getMotif()) ;

		// supprime une ligne
		clicSupprLigneHF(holder.supprLigne, index);

		return convertView ;
	}

	/**
	 * Clic pour supprimer une ligne HF de la liste
	 * @param btn
	 * @param index
	 */
	public void clicSupprLigneHF(Button btn, final int index) {
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				AlertDialog.Builder alert = new AlertDialog.Builder(context);

				alert.setTitle("Attention");
				alert.setMessage("Etes vous sur de vouloir supprimer cet item ?");
				alert.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Global.listFraisMois.get(key).supprFraisHf(index);
						Serializer.serialize(Global.filename, Global.listFraisMois, context);
						notifyDataSetChanged();
					}
				});
				alert.setNegativeButton("Non", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alert.show();
			}
		});
	}
	
}
