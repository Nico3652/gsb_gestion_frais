CREATE TABLE fraiskilometrique(
	id int(2) NOT NULL auto_increment,
	moteur char(40),
	prix_kilometrique float(4),
	CONSTRAINT pk_fraiskilometrique PRIMARY KEY (id)
) ENGINE=InnoDB;
​
ALTER TABLE fraiskilometrique ADD COLUMN idFraisForfait char(3) DEFAULT 'KM'; 
​
ALTER TABLE fichefrais ADD COLUMN id_fraiskilometrique int(2);
ALTER TABLE fichefrais ADD CONSTRAINT fk_fichefrais_fraiskilometrique FOREIGN KEY (id_fraiskilometrique) REFERENCES fraiskilometrique(id);
​
ALTER TABLE utilisateur ADD COLUMN id_fraiskilometrique int(2);
ALTER TABLE utilisateur ADD CONSTRAINT fk_utilisateur_fraiskilometrique FOREIGN KEY (id_fraiskilometrique) REFERENCES fraiskilometrique(id);
​
UPDATE utilisateur SET id_fraiskilometrique = 1 where id = 'f4';



--
-- Contenu de la table 'fraiskilometrique'
--

INSERT INTO fraiskilometrique (id,moteur,prix_kilometrique) VALUES
			('Véhicule 4CV Diesel',0.52),
			('Véhicule 5/6CV Diesel',0.58),
			('Véhicule 4CV Essence',0.62),
			('Véhicule 5/6CV Essence',0.67);