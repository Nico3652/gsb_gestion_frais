-- Création d'une table "type" 
CREATE TABLE type (id char(1), libelleType char(30), 
CONSTRAINT pk_type PRIMARY KEY (id))
ENGINE=InnoDB;
; 

 -- Ajout des données dans la table "type"
 INSERT INTO type (id,libelleType)   
 VALUES ('V', 'Visiteur médical'),         
 		('C', 'Comptable'); 

 -- Changement de nom de table 
 ALTER TABLE visiteur RENAME TO utilisateur;

 -- Ajout du champ dans utilisateur 
 ALTER TABLE utilisateur ADD COLUMN idType char(1); 

 -- Déclaration de tous les utilisateurs actuels comme des "visiteurs"
 UPDATE utilisateur SET idType = 'V'; 

 -- Déclaration de la clé étrangère "idType" en référence à "id" de la table "type"
 ALTER TABLE utilisateur ADD CONSTRAINT fk_type FOREIGN KEY (idType)
 REFERENCES type (id); 